import os
import pandas as pd 
import urllib
import urllib.request
import xarray 
from astropy import units as u
from collections import OrderedDict
from sunpy.time import parse_time
from sunpy import timeseries as ts 


APP_NAME  = 'nasa_spaceApp'
DATA_PATH = f'{APP_NAME}/data'
BASE_URL  = 'https://data.ngdc.noaa.gov/platforms/solar-space-observing-satellites/goes/'
NOAA_URL  = 'https://services.swpc.noaa.gov/json/goes/primary'


def get_goes(date, sat=16, type='xrs', path=None):
    """
    Function to download the new GOES 16/17/18 XRS level 2 science 1s data
    
    Parameters
    ----------
    date : `str`
        date of observation - e.g. '2017-09-10'
    
    type : `str`
        satellite type. Allowed values: 'ephe', 'xrs-x1s', 'xrs-sum', 'xrs-loc', 'xrs-det', 'xrs-kd1', 'euv-avg1d', 'euv-avg1m', 'mag-hi'

    sat : `int`, optional
        satellite number, can be 16 or 17 or 18. Default is 16
    
    path : `str` 
        path to save file to. Default to cwd.
    
    Returns
    -------
    if file is downloaded or already exists it will return filename/path

    """

    if type == 'xrs-x1s':
        path_one = 'xrsf-l2-flx1s_science'
        path_two = 'sci_xrsf-l2-flx1s_g'
        version = '_v2-1-0'
    elif type == 'xrs-sum':
        path_one = 'xrsf-l2-flsum_science'
        path_two = 'sci_xrsf-l2-flsum_g'
        version = '_v2-1-0'
    elif type == 'xrs-loc':
        path_one = 'xrsf-l2-flloc_science'
        path_two = 'sci_xrsf-l2-flloc_g'
        version = '_v2-1-0'        
    elif type == 'xrs-det':
        path_one = 'xrsf-l2-fldet_science'
        path_two = 'sci_xrsf-l2-fldet_g'
        version = '_v2-1-0'
    elif type == 'xrs-kd1':
        path_one = 'xrsf-l2-bkd1d_science'
        path_two = 'sci_xrsf-l2-bkd1d_g'
        version = '_v2-1-0'
    elif type == 'ephe':
        path_one = 'ephe-l2-orb1m'
        path_two = 'dn_ephe-l2-orb1m_g'
        version = '_v0-0-3'
    elif type == 'euv-avg1d':
        path_one = 'euvs-l2-avg1d_science'
        path_two = 'sci_euvs-l2-avg1d_g'
        version = '_v1-0-1'
    elif type == 'euv-avg1m':
        path_one = 'euvs-l2-avg1m_science'
        path_two = 'sci_euvs-l2-avg1m_g'
        version = '_v1-0-1'        
    elif type == 'mag-hi':
        path_one = 'magn-l2-hires'
        path_two = 'dn_magn-l2-hires_g'
        version = '_v1-0-1' 
    else:
        path_one = 'xrsf-l2-flx1s_science'
        path_two = 'sci_xrsf-l2-flx1s_g'
        version = '_v2-1-0'
   
    date = parse_time(date)
    url = os.path.join(
        BASE_URL, 
        "goes{:d}/l2/data/{}/{:s}/{:s}/{}{:d}_d{:s}{}.nc".format(
                sat, 
                path_one,
                date.strftime('%Y'),
                date.strftime('%m'), 
                path_two,
                sat,
                date.strftime('%Y%m%d'),
                version
            )
        )

    if path is not None:
        filename = os.path.join(path, url.split('/')[-1])
    else:
        filename = url.split('/')[-1]
        
    if os.path.exists(filename):
        return filename
    
    else:
        try:
            urllib.request.urlretrieve(url, filename)

            if os.path.exists(filename):
                return filename
            
        except:
            print("Download failed")
            return None


def make_goes_timeseries(file):
    """
    Function to read in the new GOES 16&17 level2 science data
    and return a sunpy.timeseries
    
    Parameters
    ----------
    file : `str`
        GOES netcdf file 
    
    Returns
    -------
    `sunpy.timeseries.TimeSeries`
    
    """
    data = xarray.open_dataset(file)
    
    units = OrderedDict([('xrsa', u.W/u.m**2), ('xrsb', u.W/u.m**2)])
    
    xrsb = data['xrsb_flux']
    xrsa = data['xrsa_flux']

    tt = parse_time(data['time']).datetime
    
    data = pd.DataFrame({'xrsa': xrsa, 'xrsb': xrsb}, index=tt)
    data.sort_index(inplace=True)
    
    return ts.TimeSeries(data, units)            


def read_recent_noaa(interval="7-day"):
    """
    Function to read the NOAA recent GOES-16 XRS data and return a 
    sunpy.timeseries
    
    Parameters
    ----------
    interval : `str`, optional - default past 7 days
        past interval file to read.
        
    Returns
    -------
    sunpy.timeseries
    
    """
    dict_interval = {
        "7-day": "xrays-7-day.json", 
        "3-day": "xrays-3-day.json", 
        "1-day": "xrays-1-day.json",
        "6-hour": "xrays-6-hour.json"
    }
    
    if interval not in dict_interval.keys():
        print("interval must be one of", dict_interval.keys())
    
    noaa_file = "{}/{:s}".format(NOAA_URL, dict_interval[interval])
    units = OrderedDict([('xrsa', u.W/u.m**2), ('xrsb', u.W/u.m**2)])

    data = pd.read_json(noaa_file)

    data_short = data[data['energy']=='0.05-0.4nm']
    data_long = data[data['energy']=='0.1-0.8nm']
    time_array = [parse_time(x).datetime for x in data_short['time_tag'].values]

    df = pd.DataFrame({'xrsa': data_short['flux'].values, 'xrsb': data_long['flux'].values}, index=time_array)
    df.sort_index(inplace=True)
    
    return ts.TimeSeries(df, units)


def get_geos_by_date():
    """
    Function to execute GEOS data by date.
    
    Parameters
    ----------
    None
        
    Returns
    -------
    None. Files will be created.
    
    """
    satList  = [16]             #[16, 17, 18]
    typeList = ['ephe']         #['ephe', 'xrs-x1s', 'xrs-sum', 'xrs-loc', 'xrs-det', 'xrs-kd1', 'euv-avg1d', 'euv-avg1m', 'mag-hi']
    dateList = ['2022-09-28']   #['2022-09-28', '2022-09-27', '2022-09-26', '2022-09-25', '2022-09-24', '2022-09-23', '2022-09-22', '2022-09-21', '2022-09-20']

    for type in typeList:
        for sat in satList:
            for date in dateList:
                output_file = f"{DATA_PATH}/goes-{sat}-{type}-{date.replace('-', '')}.json"

                files = get_goes(date=date, sat=16, path=DATA_PATH)

                goes_xrs = make_goes_timeseries(files)
                goes_16 = ts.TimeSeries(goes_xrs, concat=True)
                
                goes_table = goes_16.to_table()
                goes_table.write(output_file, format='pandas.json', overwrite=True)


def get_geos_last_7d():
    """
    Function to execute GEOS last 7 days data.
    
    Parameters
    ----------
    None
        
    Returns
    -------
    None. Files will be created.
    
    """
    output_file = f"{DATA_PATH}/result.json"

    goes_xrs_7days = read_recent_noaa()
    goes = ts.TimeSeries(goes_xrs_7days, concat=True)

    goes_table = goes.to_table()
    goes_table.write(output_file, format='pandas.json', overwrite=True)



#get_geos_by_date()
#get_geos_last_7d